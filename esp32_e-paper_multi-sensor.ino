/**
 *  @filename   :   ShowTemperatureHumidityOnEPaper.ino
 *  @brief      :   display temparature and humidity from DHT22 on 1.54inch e-paper display
 *  @author     :   Tomas Jankovic [gandy@gandy.sk]
 *
 *  April 2022
 */

#include <SPI.h>
#include <EPD1in54.h>
#include <EPDPaint.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <WiFi.h>
#include "time.h"
#include "secret.h"
#include <PubSubClient.h>
#define DHTPIN 2
#define DHTTYPE    DHT22     // DHT 22 (AM2302)
#define COLORED     0
#define UNCOLORED   1

unsigned char image[1024];
EPDPaint paint(image, 0, 0);    // width should be the multiple of 8
EPD1in54 epd(33, 25, 26, 27); // reset, dc, cs, busy
DHT dht(DHTPIN, DHTTYPE);

const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 3600;
const char* mqtt_server = "10.10.10.200";
const char* channel = "sensor-office-epaper";
float last_temperature;
float last_humidity;
int i;

WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  Serial.begin(115200);

  Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
  }
  Serial.println(" CONNECTED");

  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  
  epd.init(lutFullUpdate);
  epd.clearFrameMemory(0xFF);   // bit set = white, bit reset = black
  epd.displayFrame();
  epd.clearFrameMemory(0xFF);   // bit set = white, bit reset = black
  epd.displayFrame();

  paint.setRotate(ROTATE_0);
  paint.setWidth(200);
  paint.setHeight(24);
  
  epd.displayFrame();

  dht.begin();

  client.setServer(mqtt_server, 1883);

  //WiFi.disconnect(true);
  //WiFi.mode(WIFI_OFF);
}

void loop() {
  Serial.println("DEBUG: loop");
  if (WiFi.status() != WL_CONNECTED) {
      Serial.printf("Reconecting to %s ", ssid);
      WiFi.begin(ssid, password);
      i = 0;
      while (WiFi.status() != WL_CONNECTED) {
          delay(500);
          if (i++ > 10) {
            break;
          }
      }
  }
  client.connect("ESP32Client");
  client.subscribe(channel);
  client.loop();
  updateData();
  client.disconnect();
  delay(58000);
}

void updateData() {
  char timedata[50];
  char temperature[10];
  char humidity[10];
  int update_data = 0;
  char wifi_state_text[10] = "WIFI: !!";
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("DEBUG: wifi OK");
    sprintf(wifi_state_text, "WIFI: OK");
  }
  
  float actual_temperature = dht.readTemperature();
  sprintf(temperature, "%0.2f C", actual_temperature);
  delay(2000);
  float actual_humidity = dht.readHumidity();
  sprintf(humidity, "%0.2f %%", actual_humidity);

  Serial.printf("DEBUG: temperature -> last: %0.2f C; actual: %0.2f C\n", last_temperature, actual_temperature);
  Serial.printf("DEBUG: humidity -> last: %0.2f %%; actual: %0.2f %%\n", last_humidity, actual_humidity);

  if(isnan(last_temperature)) {
    last_temperature = actual_temperature;
  } else {
    if(abs(last_temperature - actual_temperature) >= 0.2f) {
      update_data = 1;
      last_temperature = actual_temperature;
    }
  }

  if(isnan(last_humidity)) {
    last_humidity = actual_humidity;
  } else {
    if(abs(last_humidity - actual_humidity) >= 0.2f) {
      update_data = 1;
      last_humidity = actual_humidity;
    }
  }

  String json = ("{\"humidity\": " + String(actual_humidity) + ", \"temperature\": " + String(actual_temperature) + "}");
  client.publish(channel, json.c_str(), true);

  Serial.printf("DEBUG: udadate data = %i \n", update_data);
  if(update_data) {
    struct tm timeinfo;
    if(!getLocalTime(&timeinfo)){
      Serial.println("Failed to obtain time");
    } else {
      strftime(timedata, 50, "updated at: %H:%M %d.%m.%Y", &timeinfo);
    }
    Serial.printf("DEBUG: timedata = %s \n", timedata);
  
    paint.setRotate(ROTATE_270);
    paint.setWidth(24);
    paint.setHeight(200);

    paint.clear(UNCOLORED);
    paint.drawStringAt(5, 4, timedata, &Font12, COLORED);
    epd.setFrameMemory(paint.getImage(), 0, 0, paint.getWidth(), paint.getHeight());

    paint.clear(UNCOLORED);
    paint.drawStringAt(40, 4, temperature, &Font24, COLORED);
    epd.setFrameMemory(paint.getImage(), 60, 0, paint.getWidth(), paint.getHeight());

    paint.clear(UNCOLORED);
    paint.drawStringAt(40, 4, humidity, &Font24, COLORED);
    epd.setFrameMemory(paint.getImage(), 110, 0, paint.getWidth(), paint.getHeight());

    paint.clear(UNCOLORED);
    paint.drawStringAt(5, 4, wifi_state_text, &Font16, COLORED);
    epd.setFrameMemory(paint.getImage(), 180, 0, paint.getWidth(), paint.getHeight());

    epd.displayFrame();
  }
}
